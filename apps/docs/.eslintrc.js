module.exports = {
  root: true,
  extends: ['@chara_/eslint-config/next.js'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: true,
  },
}
