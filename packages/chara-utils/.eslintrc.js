module.exports = {
  root: true,
  extends: ['@chara_/eslint-config/library.js'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: true,
  },
}
