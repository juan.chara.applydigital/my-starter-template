export type OrNavbarProps = {
  children?: React.ReactNode
}

export function OrNavbar({ children }: OrNavbarProps) {
  return <nav id="nav">{children}</nav>
}

OrNavbar.displayName = 'OrNavbar'
