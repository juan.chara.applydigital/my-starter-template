import { ImgHTMLAttributes } from 'react'

export type AtImageProps = ImgHTMLAttributes<HTMLImageElement>

export function AtImage(props: AtImageProps) {
  return <img {...props} />
}

AtImage.displayName = 'AtImage'
