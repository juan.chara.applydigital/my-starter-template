import { ButtonHTMLAttributes } from 'react'

export type AtButtonProps = ButtonHTMLAttributes<HTMLButtonElement>

export function AtButton({ children, ...rest }: AtButtonProps) {
  return <button {...rest}>{children}</button>
}

AtButton.displayName = 'AtButton'
