import { AtButton, AtButtonProps, AtImage, AtImageProps } from '../../atoms'

export type MlCardProps = {
  images?: AtImageProps[]
  children: React.ReactNode
  actionButtons?: AtButtonProps[]
}

export function MlCard({ images = [], children, actionButtons = [] }: MlCardProps) {
  return (
    <div>
      {images?.map((image) => <AtImage {...image} />)}
      {children}
      {actionButtons?.map((button) => <AtButton {...button} />)}
    </div>
  )
}

MlCard.displayName = 'MlCard'
