module.exports = {
  root: true,
  extends: ['@chara_/eslint-config/react-internal.js'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: true,
  },
}
