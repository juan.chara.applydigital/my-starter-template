# @chara\_/core

## 2.1.1

### Patch Changes

- change navbar

## 2.1.0

### Minor Changes

- 1e88307: add atomic-design structure for @chara\_/core library

## 2.0.0

### Major Changes

- patch

## 1.0.0

### Major Changes

- send major
